// SPDX-FileCopyrightText: 2022 The Manyverse Authors
//
// SPDX-License-Identifier: CC0-1.0

export default [
  'Dace',
  'C Moid',
  'Andrew Lewman',
  'DC Posch',
  'Irakli Gozalishvili',
];
